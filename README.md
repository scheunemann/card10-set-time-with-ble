# CARD10 set time with BLE

This script gets the current time in milli seconds, connects to the CARD10 wrist badge and writes to the [time update characteristic](https://firmware.card10.badge.events.ccc.de/bluetooth/card10.html#time-update-characteristic).

## Script usage

```python
python run.py [MAC="CA:4D:10:C3:20:19"] [MIN_OFFSET=0]
```

- MAC: If your Card10 MAC isn't `CA:4D:10:C3:20:19` (WHYYY?), then provide your MAC as a first command line argument for connection
- MIN_OFFSET: for people not living in the CCC timezone (did I miss an option to set the timezone somewhere?) or for people who like to tweak their time so they never be late again, OFFSET changes the system time (of wherever you run this script) with the provided offset minutes

For example, running `python run.py CA:4D:10:AA:BB:CC -60` changes the Card10 wrsit badge CA:4D:10:AA:BB:CC to current UK time.

## Requirements
- [bluepy](https://github.com/IanHarvey/bluepy)

