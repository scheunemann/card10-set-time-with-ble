# run.py [MAC] [offset in min]

import sys

import bluepy.btle as btle
from datetime import datetime

if (len(sys.argv) > 1):
	mac = str(sys.argv[1])
else:
	mac = "CA:4D:10:C3:20:19"

if (len(sys.argv) > 2):
	offset = int(sys.argv[2])
else:
	offset = 0

print('connecting to %s' % mac)
p = btle.Peripheral('CA:4D:10:EA:7F:0B')
print('connected')

c = p.getCharacteristics(uuid="42230201-2342-2342-2342-234223422342")[0]

# seconds since unix
utc_time_s = (datetime.utcnow() - datetime(1970, 1, 1)).total_seconds()

# milliseconds [+ offset]
time_ms = int((utc_time_s + offset * 60) * 1000)

c.write(time_ms.to_bytes(8, byteorder='big'))
print('time set')


p.disconnect()
print('disconnected')